package circuit_breaker

import (
	"fmt"
	"net/http"
	"time"
)

const (
	defaultTimeout = 60
	defaultTries   = 3
)

type state uint

const (
	OPEN state = iota
	HALFOPEN
	CLOSED
)

type httpDoer interface {
	Do(req *http.Request) (*http.Response, error)
}

type CircuitBreakerOpt func(*CircuitBreaker)

type CircuitBreaker struct {
	timeout     int64
	maxTries    int
	state       state
	failedAt    int64
	triesFailed int
	triesPass   int
	httpClient  httpDoer
}

func WithTimeout(timeout int64) func(*CircuitBreaker) {
	return func(cb *CircuitBreaker) {
		cb.timeout = timeout
	}
}

func WithTries(tries int) func(*CircuitBreaker) {
	return func(cb *CircuitBreaker) {
		cb.maxTries = tries
	}
}

func WithHttpClient(httpClient *http.Client) func(breaker *CircuitBreaker) {
	return func(cb *CircuitBreaker) {
		cb.httpClient = httpClient
	}
}

func NewCircuitBreaker(opts ...CircuitBreakerOpt) *CircuitBreaker {
	cb := &CircuitBreaker{
		timeout:     defaultTimeout,
		maxTries:    defaultTries,
		httpClient:  http.DefaultClient,
		state:       CLOSED,
		failedAt:    0,
		triesFailed: 0,
		triesPass:   0,
	}

	for _, opt := range opts {
		opt(cb)
	}

	return cb
}

func (cb *CircuitBreaker) Do(req *http.Request) (*http.Response, error) {
	now := time.Now().Unix()
	if cb.state == OPEN && cb.failedAt+cb.timeout <= now {
		return nil, fmt.Errorf("circuit is open, returning immidietly")
	}

	// timeout passed, let's try again
	resp, err := cb.httpClient.Do(req)
	if err != nil {
		return resp, err
	}

	if resp.StatusCode >= 500 && resp.StatusCode <= 511 {
		cb.onFail()
		return resp, err
	}

	cb.onPass()
	return resp, nil
}

func (cb *CircuitBreaker) onFail() {
	cb.triesPass = 0
	switch cb.state {
	case OPEN:
		// was open, but timeout window passed and it failed the retry
		// reset failedAt and triesFail counters
		cb.failedAt = time.Now().Unix()
		cb.triesFailed += 1
	case HALFOPEN:
		// was half open and failed - open fully again and reset counter
		cb.state = OPEN
		cb.failedAt = time.Now().Unix()
		cb.triesFailed += 1
	case CLOSED:
		cb.triesFailed += 1
		cb.triesPass = 0
		// was closed but failed too many times, open it
		if cb.triesFailed >= cb.maxTries {
			cb.state = OPEN
			cb.failedAt = time.Now().Unix()
		}
	}
}

func (cb *CircuitBreaker) onPass() {
	switch cb.state {
	case OPEN:
		// was open but passed, so half open it
		cb.state = HALFOPEN
		cb.triesPass = 1
		cb.triesFailed = 0
	case HALFOPEN:
		cb.triesPass += 1
		// was half opened and passed enough times, close it
		if cb.triesPass >= cb.maxTries {
			cb.state = CLOSED
			cb.triesFailed = 0
		}
	}
}
