package circuit_breaker

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

const fakeUrl = "http://fakeUrl.org/get"

type mockDoer struct {
	resp *http.Response
	err  error
}

func (m *mockDoer) Do(r *http.Request) (*http.Response, error) {
	return m.resp, m.err
}

func withMockDoer(resp *http.Response, err error) func(cb *CircuitBreaker) {
	return func(cb *CircuitBreaker) {
		cb.httpClient = &mockDoer{
			resp: resp,
			err:  err,
		}
	}
}

func TestCbClosed(t *testing.T) {
	testBytes := `success`

	test := struct {
		resp *http.Response
		err  error
	}{
		resp: &http.Response{
			StatusCode: 200,
			Body:       ioutil.NopCloser(bytes.NewReader([]byte(testBytes))),
		},
		err: nil,
	}

	cb := NewCircuitBreaker(withMockDoer(test.resp, test.err))

	req, err := http.NewRequest("GET", fakeUrl, nil)
	assert.Nil(t, err)

	resp, err := cb.Do(req)
	defer resp.Body.Close()

	assert.Nil(t, err)
	assert.Equal(t, test.resp.StatusCode, resp.StatusCode)
	bb, err := ioutil.ReadAll(resp.Body)
	assert.Nil(t, err)
	assert.Equal(t, testBytes, string(bb))
}

func TestCbClosedToOpen(t *testing.T) {
	test := struct {
		resp  *http.Response
		err   error
		state state
	}{
		resp: &http.Response{
			StatusCode: 500,
		},
		err:   nil,
		state: OPEN,
	}

	cb := NewCircuitBreaker(withMockDoer(test.resp, test.err), WithTries(0))
	req, err := http.NewRequest("GET", fakeUrl, nil)
	assert.Nil(t, err)

	resp, err := cb.Do(req)
	assert.Nil(t, err)
	assert.Equal(t, test.resp.StatusCode, resp.StatusCode)
	assert.Equal(t, 1, cb.triesFailed)
	assert.Equal(t, test.state, cb.state)
}

func TestCbHalfOpenToClosed(t *testing.T) {
	test := struct {
		resp  *http.Response
		err   error
		state state
	}{
		resp: &http.Response{
			StatusCode: 200,
		},
		err:   nil,
		state: CLOSED,
	}

	cb := NewCircuitBreaker(withMockDoer(test.resp, test.err), WithTries(0))
	cb.state = HALFOPEN
	req, err := http.NewRequest("GET", fakeUrl, nil)
	assert.Nil(t, err)

	resp, err := cb.Do(req)
	assert.Nil(t, err)
	assert.Equal(t, test.resp.StatusCode, resp.StatusCode)
	assert.Equal(t, 1, cb.triesPass)
	assert.Equal(t, test.state, cb.state)
}

func TestCbHalfOpenToOpen(t *testing.T) {
	test := struct {
		resp  *http.Response
		err   error
		state state
	}{
		resp: &http.Response{
			StatusCode: 500,
		},
		err:   nil,
		state: OPEN,
	}

	cb := NewCircuitBreaker(withMockDoer(test.resp, test.err), WithTries(0))
	cb.state = HALFOPEN
	req, err := http.NewRequest("GET", fakeUrl, nil)
	assert.Nil(t, err)

	resp, err := cb.Do(req)
	assert.Nil(t, err)
	assert.Equal(t, test.resp.StatusCode, resp.StatusCode)
	assert.Equal(t, 1, cb.triesFailed)
	assert.Equal(t, test.state, cb.state)
}

func TestCbOpenToHalfOpen(t *testing.T) {
	test := struct {
		resp  *http.Response
		err   error
		state state
	}{
		resp: &http.Response{
			StatusCode: 200,
		},
		err:   nil,
		state: HALFOPEN,
	}

	cb := NewCircuitBreaker(withMockDoer(test.resp, test.err), WithTries(0))
	cb.state = OPEN
	cb.failedAt = time.Now().Unix()
	req, err := http.NewRequest("GET", fakeUrl, nil)
	assert.Nil(t, err)

	resp, err := cb.Do(req)
	assert.Nil(t, err)
	assert.Equal(t, test.resp.StatusCode, resp.StatusCode)
	assert.Equal(t, 1, cb.triesPass)
	assert.Equal(t, test.state, cb.state)
}
